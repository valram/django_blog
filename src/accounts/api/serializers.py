from django.core.exceptions import ValidationError
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.db.models import Q

from rest_framework.serializers import (
            CharField,
            EmailField,
            ModelSerializer, 
            HyperlinkedIdentityField,
            SerializerMethodField,
            )


User = get_user_model()

class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
        ]


class UserCreateSerializer(ModelSerializer):
    email = EmailField(label='Email address')
    email2 = EmailField(label='Confirm email')
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'email2',
            'password',
        ]
        extra_kwargs = {'password':
                            {'write_only': True}
                            }

    def validate_email2(self, value):
        email = self.get_initial().get('email')
        email2 = value
        if email != email2:
            raise ValidationError('Emails must match')
        user_qs = User.objects.filter(email=email2)
        if user_qs:
            raise ValidationError('User with this email already exists')
        return value

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        new_user = User(username=username, email=email)
        new_user.set_password(password)
        new_user.save()
        return validated_data


class UserLoginSerializer(ModelSerializer):
    token = CharField(allow_blank=True, read_only=True)
    username = CharField(label='User name', required=False, allow_blank=True)
    email = EmailField(label='Email address', required=False, allow_blank=True)
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'token',
        ]
        extra_kwargs = {'password':
                            {'write_only': True}
                            }

    def validate(self, data):
        user_obj = None
        username = data.get('username', None)
        email = data.get('email', None)
        password = data.get('password', None)
        if not username or not email:
            raise ValidationError('Username or email must exist')
        user = User.objects.filter(
                    Q(email=email) |
                    Q(username=username)
            ).distinct()
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError('This username/email is not valid.')
        if user_obj:
            if  not user_obj.check_password(password):
                raise ValidationError('Incorrect credentials. Please try again.')
        token = 'Some random token'

        return data