from django.db.models import Q


from rest_framework.generics import (
        DestroyAPIView,
        ListAPIView,
        RetrieveAPIView,
        RetrieveUpdateAPIView,
        CreateAPIView,
        )
from rest_framework.filters import (
        SearchFilter,
        OrderingFilter,
        )
from rest_framework.pagination import (
        LimitOffsetPagination,
        PageNumberPagination,
        )
from rest_framework.permissions import (
        AllowAny,
        IsAdminUser,
        IsAuthenticated,
        IsAuthenticatedOrReadOnly,
        )


from posts.models import Post 
from .pagination import PostPageNumberPagination
from .permissions import IsOwnerOrReadOnly
from .serializers import (
        PostListSerializer,
        PostDetailSerializer, 
        PostCreateUpdateSerializer,
        )




class PostCreateAPIView(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostCreateUpdateSerializer

    def perfom_create(self, serializer):
        serializer.save(user=self.request.user)


class PostUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostCreateUpdateSerializer
    permission_classes = [IsOwnerOrReadOnly, ]
    lookup_field = 'slug'

    def perfom_update(self, serializer):
        serializer.save(user=self.request.user)


class PostDeleteAPIView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    permission_classes = [IsOwnerOrReadOnly, ]
    lookup_field = 'slug'


class PostDetailAPIView(RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer
    lookup_field = 'slug'


class PostListAPIView(ListAPIView):
    serializer_class = PostListSerializer
    filter_backend = [SearchFilter,]
    search_fields = ['title', 'content', 'user__first_name' ]
    pagination_class = PostPageNumberPagination
    permission_classes = [AllowAny] 


    def get_queryset(self, *args, **kwargs):
        queryset_list = Post.objects.all()
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(
                            Q(title__icontains=query)|
                            Q(content__icontains=query)|
                            Q(user__first_name__icontains=query)|
                            Q(user__last_name__icontains=query)
                            ).distinct()
        return queryset_list






