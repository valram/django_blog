from rest_framework.serializers import (
            ModelSerializer, 
            HyperlinkedIdentityField,
            SerializerMethodField,
            )


from accounts.api.serializers import UserDetailSerializer
from posts.models import Post
from comments.api.serializers import CommentSerializer
from comments.models import Comment


post_delete_url = HyperlinkedIdentityField(
            view_name='posts-api:delete',
            lookup_field='slug',
            )
post_detail_url = HyperlinkedIdentityField(
            view_name='posts-api:detail',
            lookup_field='slug',
            )

class PostCreateUpdateSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = [
            'title', 
            'content',
            'publish',
        ]

class PostDetailSerializer(ModelSerializer):
    delete_url = post_delete_url
    user = UserDetailSerializer(read_only=True)
    image = SerializerMethodField()
    html = SerializerMethodField()
    comments = SerializerMethodField()
    class Meta:
        model = Post
        fields = [
            'id',
            'user',
            'title', 
            'content',
            'html',
            'publish',
            'delete_url',
            'image',
            'comments',
        ]
    def get_comments(self, obj):
        comm_qs = Comment.objects.filter_by_instance(obj)
        comments  = CommentSerializer(comm_qs, many=True).data
        return comments

    def get_html(self, obj):
        return obj.get_markdown()

    def get_image(self, obj):
        try:
            image = obj.image.url
        except Exception as e:
            image = None
        return image


class PostListSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    url = post_detail_url
    delete_url = post_delete_url
    user = SerializerMethodField()

    class Meta:
        model = Post
        fields = [
            'id',
            'slug',
            'url',
            'user',
            'title', 
            'content',
            'publish',
            'delete_url',
        ]






