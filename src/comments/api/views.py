from django.db.models import Q


from rest_framework.mixins import  UpdateModelMixin, DestroyModelMixin
from rest_framework.generics import (
        DestroyAPIView,
        ListAPIView,
        RetrieveAPIView,
        RetrieveUpdateAPIView,
        CreateAPIView,
        )
from rest_framework.filters import (
        SearchFilter,
        OrderingFilter,
        )
from rest_framework.pagination import (
        LimitOffsetPagination,
        PageNumberPagination,
        )
from rest_framework.permissions import (
        AllowAny,
        IsAdminUser,
        IsAuthenticated,
        IsAuthenticatedOrReadOnly,
        )


from comments.models import Comment
from posts.api.pagination import PostPageNumberPagination
from posts.api.permissions import IsOwnerOrReadOnly
from .serializers import (
        CommentDetailSerializer, 
        CommentSerializer, 
        create_comment_serializer,
    )


class CommentCreateAPIView(CreateAPIView):
    queryset = Comment.objects.all()
    def get_serializer_class(self):
        model_type = self.request.GET.get('type')
        slug = self.request.GET.get('slug')
        parent_id = self.request.GET.get('parent_id', None)
        return create_comment_serializer(
                        model_type=model_type,
                        slug=slug, 
                        parent_id=parent_id,
                        user=self.request.user
                        )


class CommentDetailAPIView(
                            DestroyModelMixin, 
                            UpdateModelMixin, 
                            RetrieveAPIView,
                            ):
    queryset = Comment.objects.filter(id__gte=0)
    serializer_class = CommentDetailSerializer
    lookup_field = 'id'
    permission_classes = [IsOwnerOrReadOnly, ]

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class CommentListAPIView(ListAPIView):
    serializer_class = CommentSerializer
    filter_backend = [SearchFilter,]
    search_fields = [
                    'user',
                    'parent',
                    'content', 
                    'user__first_name'
                    ]
    pagination_class = PostPageNumberPagination
    permission_classes = [AllowAny]


    def get_queryset(self, *args, **kwargs):
        queryset_list = Comment.objects.all()
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(
                            Q(content__icontains=query)|
                            Q(user__first_name__icontains=query)|
                            Q(user__last_name__icontains=query)
                            ).distinct()
        return queryset_list






