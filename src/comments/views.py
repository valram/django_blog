from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render, get_object_or_404


from .models import Comment
from .forms import CommentForm

# Create your views here.
@login_required
def comment_delete(request, id):
    try:
        comment = Comment.objects.get(id=id)
    except:
        raise Http404
    if comment.user != request.user:
        # messages.success(request, "You have not permissions to view this")
        # raise Http404
        response = HttpResponse("You have not permissions to view this")
        response.status_code = 403
        return response


    if request.method == "POST":
        parent_obj_url = comment.content_object.get_absolute_url()
        # children_comments = Comment.objects.filter(parent=comment.parent)
        comment.delete()
        messages.success(request, "Comment has been deleted")
        return HttpResponseRedirect(parent_obj_url)


    context = {
        "comment" : comment,
        }
    return render(request, "confirm_delete.html", context)

def comment_thread(request, id):
    try:
        comment = Comment.objects.get(id=id)
    except:
        raise Http404    
    initial_data={
        'content_type' : comment.content_type,
        'object_id' : comment.object_id,
    }
    form = CommentForm(request.POST or None, initial=initial_data)
    if form.is_valid() and request.user.is_authenticated():
        c_type = form.cleaned_data.get('content_type')
        content_type = ContentType.objects.get(model=c_type)
        object_id = form.cleaned_data.get('object_id')
        content = form.cleaned_data.get('content')
        parent_obj = None;
        try:
            parent_id = int(request.POST.get('parent_id'))
        except:
            parent_id = None
        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count() == 1:
                parent_obj = parent_qs.first()
        new_comment, created = Comment.objects.get_or_create(
                        user = request.user,
                        content_type = content_type,
                        content = content,
                        object_id = object_id,
                        parent = parent_obj,
                    )
        return HttpResponseRedirect(new_comment.content_object.get_absolute_url())
    context = {
        "comment" : comment,
        "comment_form" : form,
        }
    return render(request, "comment_thread.html", context)